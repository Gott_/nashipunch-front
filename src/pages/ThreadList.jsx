import React from "react";
import { Link } from 'react-router-dom';

import { getThreadList } from '../services/threads'

class ThreadList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      threads: []
    };
  }

  async componentDidMount() {
    const { list } = await getThreadList();

    this.setState({threads: list})
  }

  render() {
    const { threads } = this.state;

    return (
      <div>
        <h2>Threads:</h2>
        <ul>
          {threads.map(thread => (
            <Link to={`/thread/${thread.id}`} key={thread.id+thread.title}>
              <li>{thread.title}</li>
            </Link>
          ))}
        </ul>
      </div>
    );
  }
}

export default ThreadList;