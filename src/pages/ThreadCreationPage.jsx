import React from 'react';
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'

import ThreadCreation from '../components/ThreadCreation';

const ThreadCreationPage = ({user}) => {
    if(!user.token){
        return <Redirect to="/" />
    }
    return (<section>
        <ThreadCreation />
    </section>)
}

const mapStateToProps = ({ user }) => {
    return {
      user
    }
  }
  export default connect(mapStateToProps)(ThreadCreationPage)