import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

import { updateUser, saveUserToStorage, loadUserFromStorage } from "../services/user";
import updateUserAction from "../state/user/actions";

const submitUser = async ({ event, updateUserState }) => {
  event.preventDefault();
  const { username, avatar_url } = event.target;

  try {
    await updateUser({ username: username.value, avatar_url: avatar_url.value });

    const oldUser = loadUserFromStorage();
    const newUser = {
      ...oldUser,
      user: { ...oldUser.user, username: username.value, avatar_url: avatar_url.value }
    };

    saveUserToStorage(newUser);

    updateUserState({ username: username.value, avatar_url: avatar_url.value });
  } catch (err) {
    console.log("Error: could not update user.");
    console.error(err);
  }
};

const UserSetup = props => {
  if (props.user.username) {
    return <Redirect to="/" />;
  }

  return (
    <section>
      <h2>User Setup</h2>

      <h4>Looks like you have an account but haven't finished account setup yet.</h4>

      <form onSubmit={e => submitUser({ event: e, updateUserState: props.updateUserState })}>
        <p>
          <b>Username:</b>
        </p>
        <input type="text" name="username" placeholder="ThreadKillerXxX" />
        <p>
          <b>Avatar URL</b> (link to an image that's 80x80 or smaller - bigger will get you
          permabanned):
        </p>
        <input type="text" name="avatar_url" placeholder="https://image.com/image.jpg" />

        <br />
        <br />

        <button type="submit">Submit</button>
      </form>
    </section>
  );
};

const mapStateToProps = ({ user }) => {
  return {
    user
  };
};

const mapDispatchToProps = dispatch => ({
  updateUserState: data => dispatch(updateUserAction(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserSetup);
