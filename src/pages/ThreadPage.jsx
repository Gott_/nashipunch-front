import React from "react";
import { withRouter, Link } from 'react-router-dom';

import { getThreadWithPosts } from '../services/threads';

import QuickReplyBox from '../components/QuickReplyBox'
import Post from "../components/Post";

class ThreadPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      thread: {
        posts: [],
        user: {
          username: '',
          usergroup: 0
        }
      }
    };

    this.threadId = this.props.match.params.id;
  }

  componentDidMount = async () => {
    this.refreshPosts();
  }

  refreshPosts = async () => {
    const thread = await getThreadWithPosts(this.threadId);

    const newThread = {
      ...thread,
      posts: [...thread.posts].sort((a,b) => {
        return a.id - b.id
      })
    }

    this.setState({ thread: newThread });
  }

  render() {
    const { thread } = this.state;
    const { posts } = thread;
    
    return (
      <div>
        <Link to='/'>{'< Back to thread list'}</Link>
        <h2>{thread.title || '...'}</h2>
        <h4>by {thread.user.username || '...'}</h4>
        <br/>
        <p>Posts:</p>
        <ul style={{paddingLeft: 0}}>
          {posts.map(post => (
            <Post
              username={post.user.username}
              avatarUrl={post.user.avatar_url}
              userJoinDate={post.user.created_at}
              postBody={post.content.body}
              postDate={post.created_at}
            />
          ))}
        </ul>

        <br/>

        <QuickReplyBox threadId={this.props.match.params.id} callbackFn={this.refreshPosts} />

        <br/>
        Debug:
        <pre>
          {JSON.stringify(thread)}
        </pre>
      </div>
    );
  }
}

export default withRouter(ThreadPage);