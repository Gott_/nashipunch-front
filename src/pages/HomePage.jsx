import React from 'react';
import { Link } from 'react-router-dom';

import { ThreadList } from '../routes/routes';

const HomePage = () => <div>
    <Link to="/thread/new">
        <button>Create New Thread</button>
    </Link>

    <ThreadList />
</div>

export default HomePage
