import {authPut} from "./common";

export const updateUser = data => {
  return authPut({ url: "/user", data });
};

export const loadUserFromStorage = () => {
  const token = JSON.parse(sessionStorage.getItem("npusrtk"));
  const user = JSON.parse(sessionStorage.getItem("user"));
  
  if(token && user) {
    const { username, avatar_url, usergroup } = user;

    return { token, user: {username, avatar_url, usergroup} };
  }
  return null
}

export const saveUserToStorage = data => {
  const { token } = data;
  const { username, avatar_url, usergroup } = data.user;

  sessionStorage.setItem("npusrtk", JSON.stringify(token));
  sessionStorage.setItem("user", JSON.stringify({ username, avatar_url, usergroup }));
};