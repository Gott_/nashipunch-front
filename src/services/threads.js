import axios from 'axios';

import {authPost} from './common';
import {submitPost} from './posts';

export const getThreadList = async () => { 
    const res = await axios.get('http://localhost:3000/thread');

    const { data } = res;

    return data
};

export const getThreadWithPosts = async (threadId) => { 
    const res = await axios.get(`http://localhost:3000/thread/${threadId}?scope=withPosts`);

    const { data } = res;

    return data
};

export const createNewThread = async (data) => {
    const thread = await authPost({url: '/thread', data});

    await submitPost({content: data.content, thread_id: thread.data.id});

    return thread.data;
}