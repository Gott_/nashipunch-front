import axios from 'axios';

import { authPost } from './common';

export const submitPost = ({
    content, thread_id
}) => {
    const requestBody ={
        content: JSON.stringify(content),
        thread_id
    };

    return authPost({url: '/post', data: requestBody});
};

export const getPostList = async () => { 
    const res = await axios.get('http://localhost:3000/post');

    const { data } = res;

    return data
};