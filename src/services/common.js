import axios from 'axios';

export const authPut = ({ url, data }) => {
    const token = sessionStorage.getItem("npusrtk");

    return axios.put(`http://localhost:3000${url}`, data, {
        headers: {
            token,
        }
    });
};

export const authPost = ({ url, data }) => {
    const token = sessionStorage.getItem("npusrtk");

    return axios.post(`http://localhost:3000${url}`, data, {
        headers: {
            token,
        }
    });
};