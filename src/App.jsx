import React from "react";
import thunk from 'redux-thunk';
import ReactDOM from "react-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import reducers from './state/reducers';

import MainView from './MainView';

const composeEnhancers =  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

const App = () => {
  return (
      <Provider store={store}>
        <BrowserRouter>
            <MainView />
        </BrowserRouter>
      </Provider>
  );
};

export default App;

ReactDOM.render(<App />, document.getElementById("app"));

