import styled from 'styled-components';

export const PostWrapper = styled.li`
    display: flex;
    width: 80%;
    border: 1px solid black;
    margin: 10px 0 10px 10px;
`;

export const UserInfo = styled.div`
    padding: 10px;  
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 150px;
    height: 100%;
`;

export const UserAvatar = styled.img`
    width: 80px;
    height: 80px;
`;

export const UserName = styled.span`
    font-weight: bold;
`;

export const UserJoinDate = styled.span`
    font-size: 10px;
`;

export const PostBody = styled.div`
    width: 100%;
`;

export const PostDate = styled.div`
    height: 30px;
    display: flex;
    justify-content: left;
    padding: 5px;
    align-items: center;
    border-bottom: 1px solid black;
`;

export const PostContent = styled.div`
    padding: 10px;
`;