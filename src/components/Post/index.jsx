import React from "react";
import dayjs from "dayjs";
import relativeTime from 'dayjs/plugin/relativeTime';

dayjs.extend(relativeTime);

import {
  PostWrapper,
  UserInfo,
  UserName,
  UserJoinDate,
  PostBody,
  PostDate,
  PostContent,
  UserAvatar
} from "./style";

const Post = ({ username, avatarUrl, userJoinDate, postBody, postDate }) => (
  <PostWrapper>
    <UserInfo>
      <UserAvatar src={avatarUrl} alt={`${username}'s Avatar`} />

      <UserName>{username}</UserName>

      <UserJoinDate title={`Joined ${dayjs(userJoinDate).format('DD/MM/YYYY')}`}>{dayjs(userJoinDate).format('MMM YYYY')}</UserJoinDate>
    </UserInfo>

    <PostBody>
      <PostDate title={dayjs(postDate).format('DD/MM/YYYY HH:MM:ss')}>{dayjs().to(dayjs(postDate))}</PostDate>

      <PostContent>{postBody}</PostContent>
    </PostBody>
  </PostWrapper>
);

export default Post