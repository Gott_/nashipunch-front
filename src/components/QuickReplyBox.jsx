import React from 'react'
import { connect } from "react-redux";

import StyledQuickReply from './style';

import { getPostList, submitPost } from '../services/posts'

class QuickReply extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
        }

        this.handleInput = this.handleInput.bind(this)
        this.submitPost = this.submitPost.bind(this)
    }

    handleInput = ({target}) => {
        this.setState({ text: target.value})
    }

    submitPost = async () => {
        const { threadId, callbackFn }= this.props;

        if(!threadId) {
            alert('No thread ID provided. That\'s an error!');
        }

        await submitPost({content: {body: this.state.text}, thread_id: threadId});
        
        this.setState({text: ''});

        callbackFn();
    }   

  render() {
    if(!this.props.user.token){
        return 'Log in to reply to this thread >:^('
    }

    return (
        <div>
            <input type="text" onChange={this.handleInput} value={this.state.text} />
            <StyledQuickReply onClick={this.submitPost}>click me im stupid</StyledQuickReply>
        </div>
    )
  }
}

const mapStateToProps = ({ user }) => {
    return {
      user
    }
  }
  
  export default connect(mapStateToProps)(QuickReply);