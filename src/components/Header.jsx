import React from "react";
import axios from "axios";
import {Redirect} from 'react-router-dom';
import { GoogleLogin } from "react-google-login";
import { connect } from "react-redux";

import updateUser from '../state/user/actions'
import { loadUserFromStorage, saveUserToStorage } from "../services/user";

class Header extends React.Component {
  state = {};

  componentDidMount = () => {
    this.loadUser();
  } 

  setToken = data => {
    saveUserToStorage({token: data.token, user: {...data}});

    return this.updateUserState(data);
  };

  responseGoogle = async response => {
    console.log("Google login successful.");

    const res = await axios.get("http://localhost:3000/auth/google", {
      headers: {
        gtoken: response.tokenId
      }
    });

    this.setToken(res.data);
  };

  loadUser() {
    const data = loadUserFromStorage();

    if(!data || !data.user || !data.token) { return; }

    const { username, avatar_url, usergroup } = data.user;
    const { token } = data;
    
    return this.updateUserState({ token, username, avatar_url, usergroup });
  }

  updateUserState(data) {
    this.props.updateUser(data);
  }

  render() {
    if (!this.props.userState.token) {
      return (
        <GoogleLogin
          clientId="369362779903-tisfmnpfi495l802bgoliu12p67vcbbq.apps.googleusercontent.com"
          buttonText="Log in with Google"
          onSuccess={this.responseGoogle}
          onFailure={() => console.log("fail!")}
        />
      );
    }
    if (this.props.userState && !this.props.userState.username) {
      return <Redirect to="/usersetup" />
    }

    const { avatar_url, username } = this.props.userState;

    return (
      <header>
        <img src={avatar_url} alt=""/>
        Logged in as: {username || '...?'}
      </header>
    );
  }
}

const mapStateToProps = ({ user }) => {
  return {
    userState: user
  }
}

const mapDispatchToProps = dispatch => ({
  updateUser: data => dispatch(updateUser(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
