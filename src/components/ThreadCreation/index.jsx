import React from "react";
import {withRouter} from 'react-router-dom'

import { createNewThread } from "../../services/threads";

class ThreadCreation extends React.Component {
  state = {
    title: "",
    content: ""
  };

  handleChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    });
  };

  handleSubmit = async e => {
    e.preventDefault();

    const { title, content } = e.target;

    if(title.length < 3 || content.length < 3){
      return;
    }

    try {
      const thread = await createNewThread({
        title: this.state.title,
        content: {
          body: this.state.content
        }
      });

      this.props.history.push(`/thread/${thread.id}`);
    } catch (err) {
        console.log('Error creating thread:');
        console.error(err); 
    }
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>

        <p><b>Title</b></p>
        <input type="text" name="title" onChange={this.handleChange} />

        <br />

        <p><b>Content</b></p>
        <input type="text" name="content" onChange={this.handleChange} />

        <br/>
        <br/>

        <button type="submit">Submit</button>
      </form>
    );
  }
}

export default withRouter(ThreadCreation);
