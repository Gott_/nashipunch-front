import styled from 'styled-components';

const StyledQuickReply = styled.button`
    background: red;
    color: white;
`;

export default StyledQuickReply;