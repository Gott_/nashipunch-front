const updateUserState = data => {
  return {
    type: "UPDATE_USER",
    data
  };
};

export default updateUserState;
