const initialState = {
  username: null,
  avatar_url: null,
  id: null,
  usergroup: null,
  created_at: null,
  token: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "UPDATE_USER":
      return { ...state, ...action.data };
    default:
      return state;
  }
};

export default userReducer;
