import * as Routes from './routes';

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: Routes.HomePage,
    exact: true,
  },
  {
    path: '/usersetup',
    name: 'UserSetup',
    component: Routes.UserSetup,
    exact: true,
  },
  {
    path: '/thread/new',
    name: 'ThreadCreationPage',
    component: Routes.ThreadCreationPage,
    exact: true,
  },
  {
    path: '/thread/:id',
    name: 'ThreadPage',
    component: Routes.ThreadPage,
    exact: true,
  },
];

export default routes;