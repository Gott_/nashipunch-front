import React from 'react';
import loadable from 'loadable-components';

import Loading from '../components/Loading';

const LoadingComponent = () => <Loading />;

export const ThreadList = loadable(() => import('../pages/ThreadList'), { LoadingComponent });
export const ThreadCreationPage = loadable(() => import('../pages/ThreadCreationPage'), { LoadingComponent });
export const ThreadPage = loadable(() => import('../pages/ThreadPage'), { LoadingComponent });
export const HomePage = loadable(() => import('../pages/HomePage'), { LoadingComponent });
export const UserSetup = loadable(() => import('../pages/UserSetup'), { LoadingComponent });