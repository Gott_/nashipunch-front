import React from "react";
import { Switch, Route } from "react-router-dom";

import Header from "./components/Header";

import routes from "./routes";

const MainView = () => {
  return (
    <div>
      <Header />
      <Switch>
        {routes.map(route => (<Route key={route.name} {...route} />))}
      </Switch>
    </div>
  );
};
export default MainView;