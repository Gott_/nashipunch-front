const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
var Visualizer = require('webpack-visualizer-plugin');

module.exports = {
  resolve: {
    extensions: [".jsx", ".js"]
  },
  entry: './src/App.jsx',
  devtool: 'source-map',
  output: {
    filename: '[name].js',
    chunkFilename: '[chunkhash:4].[hash:4].js',
    publicPath: '/',
    path: path.resolve(__dirname, './dist/client'),
  },
  devServer: {
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
            options: { minimize: true }
          }
        ]
      }
    ]
  },
  plugins: [
    new Visualizer(),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
};